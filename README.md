# DEPRECATION NOTICE

Since this package creation, it is now possible to easily add and use nugets package inside unity, using UnityNuget. I can only recommand it, instead of this package (they are automatically updated to latest nuget version).
You can check `https://github.com/xoofx/UnityNuGet`, and use `org.nuget.csvhelper`.

---

# Csv Helper

A Unity package that provide the useful CSV helper library, which is a .NET library for reading and writing CSV files. Extremely fast, flexible, and easy to use.

Find more informations about the CSV Helper library here : https://joshclose.github.io/CsvHelper/.

## Adding the Package

This package can easily be added via git url with `https://gitlab.com/Eyap/csvhelper.git`.

## License

The CSV Helper package is provided under the dual license MS-PL and Apache 2.0. See [LICENSE.txt](LICENSE.txt).


The unity packaging is released into the public domain, see [UNLICENSE](https://unlicense.org/).
